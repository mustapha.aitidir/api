<?php

namespace App\DataFixtures;

use App\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UtilisateurFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < 10; $i++) {
            $user = new Utilisateur();
            $user->setUserName('user' . $i);
            $user->setPassword('test');
            $user->setEmail('user' . $i . 'domaine.fr');

            $manager->persist($user);
        }
        $manager->flush();
    }
}
