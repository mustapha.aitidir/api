<?php

namespace App\Tests;

use App\Entity\Project;
use PHPUnit\Framework\TestCase;

class ApiProjectTest extends TestCase
{
    public function testProjectAdd(): void
    {
        $project = new Project();
        $project->setName('string');
        $this->assertTrue($project->getName() === 'string');
    }
}
