<?php

namespace App\Tests\Repository;

use App\DataFixtures\UtilisateurFixtures;
use App\Entity\Utilisateur;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class UserRepositoryTest extends WebTestCase
{
    protected $databaseTool;
    protected EntityManagerInterface $entityManager;

    public function setUp(): void
    {
        parent::setUp();
        $this->databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();
        $this->entityManager = self::getContainer()->get(EntityManagerInterface::class);
    }

    public function testcount(): void
    {
        $this->databaseTool->loadFixtures([
            UtilisateurFixtures::class
        ]);

        $users = $this->entityManager->getRepository(Utilisateur::class)->count([]);

        $this->assertEquals(10, $users);
    }
}
