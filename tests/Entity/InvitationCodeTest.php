<?php

namespace App\Tests\Entity;

use App\Entity\InvitationCode;
use DateTime;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Component\Validator\ConstraintViolation;

class InvitationCodeTest extends KernelTestCase
{

    protected $databaseTool;
    protected $validator;

    public function setUp(): void
    {
        parent::setUp();
        $this->databaseTool = static::getContainer()->get(DatabaseToolCollection::class)->get();
        $this->validator = self::getContainer()->get('validator');
    }

    public function getEntity(): InvitationCode
    {
        return (new InvitationCode())
            ->setDescription('mustapha')
            ->setCode('12345')
            ->setExpireAt(new \DateTime());
    }

    public function assertHasErros(InvitationCode $code, $number = 0)
    {
        $errors = $this->validator->validate($code);
        $messages = [];

        /** @var ConstraintViolation $error */
        foreach ($errors as $error) {
            $messages[] = $error->getPropertyPath() . '=>' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(', ', $messages));
    }
    public function testValideEntity()
    {
        $this->assertHasErros($this->getEntity(), 0);
    }


    public function testInvalideCode()
    {

        $this->assertHasErros($this->getEntity()->setCode('1AA356'), 1);
    }

    public function testInvalideCodeBlank()
    {

        $this->assertHasErros($this->getEntity()->setCode(''), 1);
    }

    public function testInvalideDescriptionBlank()
    {

        $this->assertHasErros($this->getEntity()->setDescription(''), 1);
    }

    public function testInvalideUsedCode()
    {

        $this->databaseTool->loadAliceFixture([
            dirname(__DIR__) . '/fixtures/InvitationCodeTestFixtures.yml'
        ]);
        $this->assertHasErros($this->getEntity()->setCode('54341'), 1);
    }
}
