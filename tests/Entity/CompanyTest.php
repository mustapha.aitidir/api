<?php

namespace App\Tests\Entity;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CompanyTest extends WebTestCase
{
    public function testMustDisplayPageCompany(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $client->request('GET', '/company');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Company index');
    }

    public function testMustAddNewCompany(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/company/new');
        $crawlerSaveButton = $crawler->selectButton('Save');
        $form = $crawlerSaveButton->form();
        $ui = uniqid();

        $form['company[name]'] =  'Atos' . $ui;
        $form['company[adress][city]'] =  'Fes';
        $form['company[adress][country]'] =  'Fes';
        $form['company[email]'] =  'mus@gmail.com';

        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'Atos' . $ui);
    }
}
