<?php

namespace App\Tests\Validator;

use App\Validator\EmailDomain;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Exception\MissingOptionsException;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;

class EmailDomainTest extends TestCase
{
    public function testRequiredParameters(): void
    {
        $this->expectException(MissingOptionsException::class);
        new EmailDomain();
    }


    public function testBadArrayBlokedParam(): void
    {
        $this->expectException(ConstraintDefinitionException::class);
        new EmailDomain(); 
    }
}
